
##============================== import packages ===========================##
import cv2
import numpy as np
from matplotlib import pyplot as plt


##============================== Creat a ASK window =========================##

import tkinter as tk
from tkinter import filedialog
root = tk.Tk()
root.withdraw()
file_path = filedialog.askopenfilename()


##============================== Gaussian Blur =============================##

img = cv2.imread(file_path,0)

Gaussian_thresh = cv2.GaussianBlur(img,(11,11),0)

cv2.imwrite('Step 1.jpg',Gaussian_thresh)


##============================== Binary Thresholding ============================##

ret,Binary_Thresholding = cv2.threshold(Gaussian_thresh,139,255,cv2.THRESH_BINARY)

cv2.imwrite('Step 2.jpg',Binary_Thresholding)


##============================== Dilation ========================================##

kernel_dilation = np.ones((3,3), np.uint8)

dilation = cv2.dilate(Binary_Thresholding, kernel_dilation, iterations=1)

cv2.imwrite('Step 3.jpg',dilation)


##============================== Closing ========================================##


kernel_closing = np.ones((13,13), np.uint8)

Closing = cv2.morphologyEx(dilation, cv2.MORPH_CLOSE, kernel_closing)

cv2.imwrite('Step 4.jpg',Closing)

##============================== Find Contours =================================##

final = cv2.imread('Step 4.jpg')
source = cv2.imread('cobbwi_1.jpg')
#blurred = cv2.pyrMeanShiftFiltering(final,31,91)
gray = cv2.cvtColor(final,cv2.COLOR_BGR2GRAY)
ret, threshold = cv2.threshold(gray,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

_,contours,_ = cv2.findContours(threshold, cv2.RETR_LIST,cv2.CHAIN_APPROX_NONE)

##============================== Select Boundry & draw on contours ====================================##
n = 0
for cnt in contours:
	area = cv2.contourArea(cnt)

	if (area>6000 and area <14000):
		x,y,w,h = cv2.boundingRect(cnt)
		cv2.rectangle(source,(x,y),(x+w,y+h),(0,255,0),2)
		#roi = source[y:y + h, x:x + w]
		#cv2.imwrite('Image_crop%d.jpg'%n, roi)
		n+=1


##============================== Show the image ====================================##

cv2.imwrite('Detected Row Crops.jpg',source)

plt.title('Number of Row Crops Detected = %d'%n)
plt.imshow(source)
plt.show()

