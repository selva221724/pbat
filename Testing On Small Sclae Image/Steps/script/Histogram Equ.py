import cv2
import numpy
from matplotlib import pyplot as plt
 
img = cv2.imread('cobbwi_1.jpg')
img_to_yuv = cv2.cvtColor(img,cv2.COLOR_BGR2YUV)
img_to_yuv[:,:,0] = cv2.equalizeHist(img_to_yuv[:,:,0])
hist_equalization_result = cv2.cvtColor(img_to_yuv, cv2.COLOR_YUV2BGR)
 
#cv2.imwrite('result.jpg',hist_equalization_result)

plt.plot(),plt.imshow(hist_equalization_result)
plt.show()
