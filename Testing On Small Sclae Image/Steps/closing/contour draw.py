##========================= Import Packages ====================================##

import cv2
import numpy as np
from matplotlib import pyplot as plt

##========================= Read the Images ====================================##

image = cv2.imread('frame13.jpg')
source = cv2.imread('cobbwi_1.jpg')

##========================= Find Contours ====================================##

blurred = cv2.pyrMeanShiftFiltering(image,31,91)
gray = cv2.cvtColor(blurred,cv2.COLOR_BGR2GRAY)
ret, threshold = cv2.threshold(gray,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

_,contours,_ = cv2.findContours(threshold, cv2.RETR_LIST,cv2.CHAIN_APPROX_NONE)
print('Number of Row Crops Detected %d -> '%len(contours))

##========================= Set Boundry and draw on contours ====================================##

for cnt in contours:
	area = cv2.contourArea(cnt)
	if (area>6000 and area <14000):
		x,y,w,h = cv2.boundingRect(cnt)
		cv2.rectangle(source,(x,y),(x+w,y+h),(0,255,0),2)            

##========================= Show the image ====================================##

plt.title('Number of Row Crops Detected = %d'%len(contours)),plt.imshow(source),plt.show()
    


