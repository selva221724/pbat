##============================== import packages ===========================##
import cv2
import numpy as np
from matplotlib import pyplot as plt
from functools import reduce

def Average(lst):
    return reduce(lambda a, b: a + b, lst) / len(lst)


##============================== Creat a ASK window =========================##

# import tkinter as tk
# from tkinter import filedialog
#
# root = tk.Tk()
# root.withdraw()
# file_path = filedialog.askopenfilename()
file_path='/Users/naveenraj/Downloads/Crop Counting/testingimages/verticallines.jpg'

##============================== Gaussian Blur =============================##

img = cv2.imread(file_path, 0)
Gaussian_thresh = cv2.GaussianBlur(img, (11, 11), 0)

cv2.imwrite('Step 1.jpg', Gaussian_thresh)

##============================== Binary Thresholding ============================##

ret, Binary_Thresholding = cv2.threshold(Gaussian_thresh, 139, 255, cv2.THRESH_BINARY)

cv2.imwrite('Step 2.jpg', Binary_Thresholding)

##============================== Dilation ========================================##

kernel_dilation = np.ones((3, 3), np.uint8)

dilation = cv2.dilate(Binary_Thresholding, kernel_dilation, iterations=1)

cv2.imwrite('Step 3.jpg', dilation)

##============================== Closing ========================================##


kernel_closing = np.ones((13, 13), np.uint8)

Closing = cv2.morphologyEx(dilation, cv2.MORPH_CLOSE, kernel_closing)

cv2.imwrite('Step 4.jpg', Closing)

##============================== Find Contours =================================##

final = cv2.imread('Step 4.jpg')
source = cv2.imread('testingimages/cobbwi_1.jpg')
# blurred = cv2.pyrMeanShiftFiltering(final,31,91)
gray = cv2.cvtColor(final, cv2.COLOR_BGR2GRAY)
ret, threshold = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

cv2.imwrite('Step 5.jpg', threshold)

_, contours, _ = cv2.findContours(threshold, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)

##============================== Select Boundry & draw on contours ====================================##
n = 0
v = []
rect_area = []
rect_points=[]
wd=[]
he=[]
##==============collecting values================##
for cnt in contours:
    area = cv2.contourArea(cnt)

    if (area > 1000 and area < 14000):
        area = cv2.contourArea(cnt)
        v.append(area)
        avg_area = Average(v)
        x, y, w, h = cv2.boundingRect(cnt)
        rect_area.append(w * h)
        rect_points.append([x,y,w,h])
        wd.append(w)
        he.append(h)
        # cv2.circle(source, (x,y), 5, (255, 0, 0), -1)
        # cv2.rectangle(source,(x,y),(x+w,y+h),(0,255,0),2)
        # roi = source[y:y + h, x:x + w]
        # cv2.imwrite('Image_crop%d.jpg'%n, roi)
        n += 1

##=======fidning average======##
avg_area = Average(rect_area)
avg_wd= Average(wd)
avg_he=Average(he)
print(rect_points)
rect_points=sorted(rect_points , key=lambda k: [k[0], k[1]])
print(rect_points)
print("average area", avg_area)
n = 0
# for point in rect_points:
#     x,y,w,h=point
    # cv2.circle(source,(x,y),5,(255,0,0),-1)
##================================= dublicating the detcted contours ===================================##
#
# for cnt in contours:
#     area = cv2.contourArea(cnt)
#     x, y, w, h = cv2.boundingRect(cnt)
#     temp = w * h
#
#     if (500 < temp < 12000):
#
#         ind = contours.index(cnt)
#
#
#         z = ind - 1
#         last = contours[z]
#
#         if (1000 < temp < 17000):
#             z -= 1
#             try:
#                 vv = contours[z]
#             except Exception as e:
#                 print("Exception in last inner ", e)
#                 break
#
#
#         q = ind + 1
#         next = contours[q]
#
#         if (1000 < temp < 17000):
#             q += 1
#             try:
#                 vvv = contours[q]
#             except Exception as e:
#                 print("Exception in last inner ", e)
#                 break
#
#
#         x, y, w, h = cv2.boundingRect(vv)
#         a, b, c, d = cv2.boundingRect(vvv)
#
#         y_max = round((y + b) / 2)
#         y_min = round(((y + h) + (b + d)) / 2)
#
#         centre = contours[ind]
#         g, h, j, k = cv2.boundingRect(centre)
#         cv2.rectangle(source, (g, y_max), (g + j, y_min), (0, 255, 0), 2)
#         n += 1

# for cnt in contours:
# for cnt in contours:
#     area = cv2.contourArea(cnt)
#     x, y, w, h = cv2.boundingRect(cnt)
#     cur_area = w * h
#     if cur_area<avg_area:
#         et= avg_area/cur_area
#         h=int(h*et)
#         cv2.rectangle(source, (x, y), (x + w, y + h), (0, 255, 0), 2)
#     cv2.rectangle(source, (x, y), (x + w, y + h), (0, 255, 0), 2)
# i=0
# while i< len(contours)-1:
#     area = cv2.contourArea(contours[i])
#     x,y,w,h = cv2.boundingRect((contours[i]))
#     cur_area = avg_wd * h
#     print(i)
#     if cur_area < avg_area:
#         nextarea=cv2.contourArea(contours[i+1])
#         if nextarea<avg_area:
#             et = avg_area / cur_area
#             h = int(h * et)
#             cv2.rectangle(source, (x, y), (x + w, y + h), (0, 255, 0), 2)
#             i+=1
#     cv2.rectangle(source, (x, y), (x + w, y + h), (0, 255, 0), 2)
#     i+=1
# i=0
# while i <len(rect_points)-1:
#     x,y,w,h= rect_points[i]
#     cur_area=w*h
#     if cur_area<avg_area:
#         et = avg_area / cur_area
#         h = int(h * et)
#         print(i)
#         rect_points[i]=[x,y,w,h]
#         x1,y1,w1,h1=rect_points[i+1]
#         if x-10<x1<x+avg_wd:
#             next_area=w1*h1
#             if next_area<avg_area:
#                 rect_points[i+1]=[x,y,w,h]
#     i+=1


#mark all points
for point in rect_points:
    x,y,w,h= point
    cv2.circle(source, (x, y), 10, (255, 0, 0), -1)
    # cv2.rectangle(source, (x, y), (x + w, y + h), (255, 255, 255), 2)

#======find duplicates=======#
i=0
count=0
while i<len(rect_points)-1:
    x,y,w,h=rect_points[i]
    x1, y1, w1, h1 = rect_points[i + 1]
    if (x-3)< x1 < (x + w) and y < y1 <y+avg_he-8:
        cv2.circle(source, (x, y), 10, (0, 0, 255), -1)
        cv2.circle(source, (x1, y1), 10, (0, 0, 255), -1)
        # print(x+w,rect_points[i],rect_points[i+1])
        rect_points[i]=[x,y,w,h+h1]
        rect_points[i+1]=[x,y,w,h+h1]
        count+=1
    i+=1
print(count)

#Draw rectangles
for point in rect_points:
    x,y,w,h= point
#     cv2.circle(source, (x, y), 20, (255, 0, 0), -1)
    cv2.rectangle(source, (x, y), (x + w, y + h), (255, 255, 0), 2)

plt.title('Number of Row Crops Detected = %d' % n)
plt.imshow(source)
plt.show()
cv2.imwrite('output.jpg', source)