##============================== import packages ===========================##
import cv2
import numpy as np
from matplotlib import pyplot as plt


## ==============================  Read =======================================##
img = cv2.imread('/home/tamil/Takvaviya IT/Products of Takvaviya/Synganta/RGB - Automated Crop Counting/clipped/new.tif')


## ==============================  convert to hsv =======================================##
hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)


## ============================== mask of green  =======================================##
mask = cv2.inRange(hsv, (38, 0, 0), (70, 255,255))



## ============================== slice the green  =======================================##
imask = mask>0
green = np.zeros_like(img, np.uint8)
green[imask] = img[imask]


cv2.imwrite("/home/tamil/Takvaviya IT/Products of Takvaviya/Synganta/RGB - Automated Crop Counting/Output/green.jpg", green)

## ============================== Contours detection  =======================================##

final = cv2.imread('/home/tamil/Takvaviya IT/Products of Takvaviya/Synganta/RGB - Automated Crop Counting/Output/green.jpg')
source = cv2.imread('/home/tamil/Takvaviya IT/Products of Takvaviya/Synganta/RGB - Automated Crop Counting/clipped/new.tif')
gray = cv2.cvtColor(final,cv2.COLOR_BGR2GRAY)
ret, threshold = cv2.threshold(gray,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

_,contours,_ = cv2.findContours(threshold, cv2.RETR_LIST,cv2.CHAIN_APPROX_NONE)





X=[]
Y=[]
W=[]
H=[]

n=0
for cnt in contours:
    area = cv2.contourArea(cnt)
    if (5000 < area <600000):
        x,y,w,h = cv2.boundingRect(cnt)
        cv2.rectangle(source,(x,y),(x+w,y+h),(0,255,0),2)
        X.append(x)
        Y.append(y)
        W.append(w)
        H.append(h)
        #roi = source[y:y + h, x:x + w]
        #cv2.imwrite('/home/tamil/Takvaviya IT/Products of Takvaviya/Automated Crop Counting/Detected Crops/Image_crop%d.jpg'%n, roi)
        
        n+=1
        

cv2.imwrite('/home/tamil/Takvaviya IT/Products of Takvaviya/Synganta/RGB - Automated Crop Counting//Detected Row Crops.jpg',source)

#plt.title('Number of Row Crops Detected = %d'%n)
#plt.imshow(source),plt.show()

J=[]
K=[]
for i, j,k,l in zip(X,Y,W,H):
    plus1= i+k
    plus2= j+l
    J.append(plus1)
    K.append(plus2)
    
    
from osgeo import gdal


srcImage = gdal.Open("/home/tamil/Takvaviya IT/Products of Takvaviya/Synganta/RGB - Automated Crop Counting/clipped/new.tif")
geoTrans = srcImage.GetGeoTransform()



def Pixel2world(geoMatrix, x, y):
    ulX = geoMatrix[0]
    ulY = geoMatrix[3]
    xDist = geoMatrix[1]
    yDist = geoMatrix[5]
    coorX = (ulX + (x * xDist))
    coorY = (ulY + (y *yDist))
    return coorX, coorY
    


a=[]
b=[]
for i, j in zip(X, Y):
    n,m = Pixel2world(geoTrans, i, j)
    a.append(n)
    b.append(m)
     
     
c=[]
d=[]
for i, j in zip(J, K):
    o,p = Pixel2world(geoTrans, i, j)
    c.append(o)
    d.append(p)



# create a memory layer with two points
layer =  QgsVectorLayer('Point', 'points' , "memory")


pr = layer.dataProvider() 
# add the first point
pt = QgsFeature()

Point1=[]
for i, j in zip(a, b):
    point1 = QgsPoint(i,j)
    Point1.append(point1)
    pt.setGeometry(QgsGeometry.fromPoint(point1))
    pr.addFeatures([pt])
# update extent of the layer
    layer.updateExtents()

Point2=[]
for i, j in zip(c, d):    
# add the second point
    pt = QgsFeature()
    point2 = QgsPoint(i,j)
    Point2.append(point2)
    pt.setGeometry(QgsGeometry.fromPoint(point2))
    pr.addFeatures([pt])
# update extent
    layer.updateExtents()

Point3=[]
for i, j in zip(a,d):
    point3 = QgsPoint(i,j)
    Point3.append(point3)
    pt.setGeometry(QgsGeometry.fromPoint(point1))
    pr.addFeatures([pt])
# update extent of the layer
    layer.updateExtents()
    
    
Point4=[]    
for i, j in zip(c,b):
    point4 = QgsPoint(i,j)
    Point4.append(point4)
    pt.setGeometry(QgsGeometry.fromPoint(point1))
    pr.addFeatures([pt])
# update extent of the layer
    layer.updateExtents()    

# add the layer to the canvas
    QgsMapLayerRegistry.instance().addMapLayers([layer])


layer =  QgsVectorLayer('Polygon', 'poly' , "memory")

from qgis.core import *
from qgis.gui import *

crs = layer.crs()
crs.createFromId(32615)
layer.setCrs(crs)

pr = layer.dataProvider() 
poly = QgsFeature()


for i,j,k,l in zip(Point1,Point3,Point2,Point4):
    points = [i,j,k,l]
    poly.setGeometry(QgsGeometry.fromPolygon([points]))
    pr.addFeatures([poly])
    layer.updateExtents()
    QgsMapLayerRegistry.instance().addMapLayers([layer])
    



