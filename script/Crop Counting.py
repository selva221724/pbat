import time 
import cv2
import numpy as np
from matplotlib import pyplot as plt


##============================== Gaussian Blur =============================##

img = cv2.imread('/home/tamil/Takvaviya IT/Products of Takvaviya/Automated Crop Counting/grid Input.jpg')

Gaussian_thresh = cv2.GaussianBlur(img,(11,11),0)

cv2.imwrite('/home/tamil/Takvaviya IT/Products of Takvaviya/Automated Crop Counting/Step 1.jpg',Gaussian_thresh)


##============================== Binary Thresholding ============================##

ret,Binary_Thresholding = cv2.threshold(Gaussian_thresh,139,255,cv2.THRESH_BINARY)

cv2.imwrite('/home/tamil/Takvaviya IT/Products of Takvaviya/Automated Crop Counting/Step 2.jpg',Binary_Thresholding)


##============================== Dilation ========================================##

kernel_dilation = np.ones((3,3), np.uint8)

dilation = cv2.dilate(Binary_Thresholding, kernel_dilation, iterations=1)

cv2.imwrite('/home/tamil/Takvaviya IT/Products of Takvaviya/Automated Crop Counting/Step 3.jpg',dilation)


##============================== Closing ========================================##


kernel_closing = np.ones((13,13), np.uint8)

Closing = cv2.morphologyEx(dilation, cv2.MORPH_CLOSE, kernel_closing)

cv2.imwrite('/home/tamil/Takvaviya IT/Products of Takvaviya/Automated Crop Counting/Step 4.jpg',Closing)

##============================== Find Contours =================================##

final = cv2.imread('/home/tamil/Takvaviya IT/Products of Takvaviya/Automated Crop Counting/Step 4.jpg')
source = cv2.imread('/home/tamil/Takvaviya IT/R&D/raster_syngenta/c123.tif')
gray = cv2.cvtColor(final,cv2.COLOR_BGR2GRAY)
ret, threshold = cv2.threshold(gray,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
program_starts = time.time()


_,contours,_ = cv2.findContours(threshold, cv2.RETR_LIST,cv2.CHAIN_APPROX_NONE)


now = time.time()
print("It has been {0} seconds since the loop started".format(now - program_starts))
