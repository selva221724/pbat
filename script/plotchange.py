def Average(lst):
    return reduce(lambda a, b: a + b, lst) / len(lst)
##============================== Gaussian Blur =============================##

img = cv2.imread('/home/naveen/Documents/NIR_Raster/c123.tif', 0)
Gaussian_thresh = cv2.GaussianBlur(img, (11, 11), 0)

cv2.imwrite('Step 1.tif', Gaussian_thresh)

##============================== Binary Thresholding ============================##

ret, Binary_Thresholding = cv2.threshold(Gaussian_thresh, 139, 255, cv2.THRESH_BINARY)

cv2.imwrite('Step 2.tif', Binary_Thresholding)

##============================== Dilation ========================================##

kernel_dilation = np.ones((3, 3), np.uint8)

dilation = cv2.dilate(Binary_Thresholding, kernel_dilation, iterations=1)

cv2.imwrite('Step 3.tif', dilation)

##============================== Closing ========================================##


kernel_closing = np.ones((13, 13), np.uint8)

Closing = cv2.morphologyEx(dilation, cv2.MORPH_CLOSE, kernel_closing)

cv2.imwrite('Step 4.tif', Closing)

##============================== Find Contours =================================##

final = cv2.imread('Step 4.tif')
source = cv2.imread('/home/naveen/Documents/grid Input.tif')
# blurred = cv2.pyrMeanShiftFiltering(final,31,91)
gray = cv2.cvtColor(final, cv2.COLOR_BGR2GRAY)
ret, threshold = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

cv2.imwrite('Step 5.tif', threshold)

_, contours, _ = cv2.findContours(threshold, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)

##============================== Select Boundry & draw on contours ====================================##
n = 0
v = []
rect_area = []
rect_points=[]
wd=[]
he=[]
##=======good heights =====#
hal=[]
for cnt in contours:
    area = cv2.contourArea(cnt)
    if(area>32000 and area >55000):
        x, y, w, h = cv2.boundingRect(cnt)
        hal.append(h)

##==============collecting values================##
for cnt in contours:
    area = cv2.contourArea(cnt)
    if (area > 5000 and area < 50000):
        #area = cv2.contourArea(cnt)
        #v.append(area)
        #avg_area = Average(v)
        x, y, w, h = cv2.boundingRect(cnt)
        rect_area.append(w * h)
        rect_points.append([x,y,w,h])
        wd.append(w)
        he.append(h)
        # cv2.circle(source, (x,y), 5, (255, 0, 0), -1)
        # cv2.rectangle(source,(x,y),(x+w,y+h),(0,255,0),2)
        # roi = source[y:y + h, x:x + w]
        # cv2.imwrite('Image_crop%d.jpg'%n, roi)
        n += 1

##=======fidning average======##
avg_area = Average(rect_area)
avg_wd= Average(wd)
avg_he=Average(hal)
print(rect_points)
rect_points=sorted(rect_points , key=lambda k: [k[0], k[1]])
print(rect_points)
print("average area", avg_area)
n = 0
#mark all points
for point in rect_points:
    x,y,w,h= point
    cv2.circle(source, (x, y), 10, (255, 0, 0), -1)
    # cv2.rectangle(source, (x, y), (x + w, y + h), (255, 255, 255), 2)

#======find duplicates=======#
i=0
count=0
while i<len(rect_points)-1:
    x,y,w,h=rect_points[i]
    x1, y1, w1, h1 = rect_points[i + 1]
    if (x-3)< x1 < (x + w) and y < y1 <y+avg_he-8:
        cv2.circle(source, (x, y), 10, (0, 0, 255), -1)
        cv2.circle(source, (x1, y1), 10, (0, 0, 255), -1)
        # print(x+w,rect_points[i],rect_points[i+1])
        rect_points[i]=[x,y,w,h+h1+(y1-y)]
        rect_points[i+1]=[x,y,w,h+h1+(y1-y)]
        count+=1
    i+=1
print(count)

#Draw rectangles
count=0
X=[]
Y=[]
J=[]
K=[]
for point in rect_points:
    x,y,w,h= point
    X.append(x)
    Y.append(y)
    J.append(x+w)
    K.append(y+h)
    count+=1
#     cv2.circle(source, (x, y), 20, (255, 0, 0), -1)
    cv2.rectangle(source, (x, y), (x + w, y + h), (255, 255, 0), 2)
#plt.title('Number of Row Crops Detected = %d' % count)
#plt.imshow(source)
#plt.show()
cv2.imwrite('output.tif', source)
print(count)

#for point in rect_points:
#    i,j,k,l=point
#    plus1= i+k
#    plus2= j+l
#    J.append(plus1)
#    K.append(plus2)
    
