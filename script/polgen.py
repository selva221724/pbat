# create a memory layer with two points
layer =  QgsVectorLayer('Point', 'points' , "memory")
pr = layer.dataProvider() 
# add the first point
pt = QgsFeature()

Point1=[]
for i, j in zip(a, b):
    point1 = QgsPoint(i,j)
    Point1.append(point1)
    pt.setGeometry(QgsGeometry.fromPoint(point1))
    pr.addFeatures([pt])
# update extent of the layer
    layer.updateExtents()

Point2=[]
for i, j in zip(c, d):    
# add the second point
    pt = QgsFeature()
    point2 = QgsPoint(i,j)
    Point2.append(point2)
    pt.setGeometry(QgsGeometry.fromPoint(point2))
    pr.addFeatures([pt])
# update extent
    layer.updateExtents()

Point3=[]
for i, j in zip(a,d):
    point3 = QgsPoint(i,j)
    Point3.append(point3)
    pt.setGeometry(QgsGeometry.fromPoint(point1))
    pr.addFeatures([pt])
# update extent of the layer
    layer.updateExtents()
    
    
Point4=[]    
for i, j in zip(c,b):
    point4 = QgsPoint(i,j)
    Point4.append(point4)
    pt.setGeometry(QgsGeometry.fromPoint(point1))
    pr.addFeatures([pt])
# update extent of the layer
    layer.updateExtents()    

# add the layer to the canvas
    QgsMapLayerRegistry.instance().addMapLayers([layer])


layer =  QgsVectorLayer('Polygon', 'poly' , "memory")
pr = layer.dataProvider() 
poly = QgsFeature()


for i,j,k,l in zip(Point1,Point3,Point2,Point4):
    points = [i,j,k,l]
    poly.setGeometry(QgsGeometry.fromPolygon([points]))
    pr.addFeatures([poly])
    layer.updateExtents()
    QgsMapLayerRegistry.instance().addMapLayers([layer])
    



