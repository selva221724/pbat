#============================== import packages ===========================##
import cv2
import numpy as np
from matplotlib import pyplot as plt
from sklearn.cluster import KMeans


## ==============================  Read =======================================##
img = cv2.imread('/home/tamil/Takvaviya IT/Products of Takvaviya/Synganta/RGB - Automated Crop Counting/clipped/clipped.tif')

## ==============================  convert to hsv =======================================##
hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)


## ============================== mask of green  =======================================##
mask = cv2.inRange(hsv, (38, 0, 0), (70, 255,255))



## ============================== slice the green  =======================================##
imask = mask>0
green = np.zeros_like(img, np.uint8)
green[imask] = img[imask]


cv2.imwrite("/home/tamil/Takvaviya IT/Products of Takvaviya/Synganta/RGB - Automated Crop Counting/Output/green.jpg", green)

## ============================== Contours detection  =======================================##

final = cv2.imread('/home/tamil/Takvaviya IT/Products of Takvaviya/Synganta/RGB - Automated Crop Counting/Output/green.jpg')
source = cv2.imread('/home/tamil/Takvaviya IT/Products of Takvaviya/Synganta/RGB - Automated Crop Counting/clipped/new.tif')
gray = cv2.cvtColor(final,cv2.COLOR_BGR2GRAY)
ret, threshold = cv2.threshold(gray,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

_,contours,_ = cv2.findContours(threshold, cv2.RETR_LIST,cv2.CHAIN_APPROX_NONE)



n = 0
v = []
rect_area = []
rect_points=[]
X=[]
Y=[]
X1=[]
Y1=[]
wd=[]
he=[]
yp=[]
all=[]
##==============collecting values================##
for cnt in contours:
    area = cv2.contourArea(cnt)

    if (area > 50000 and area < 200000):
        area = cv2.contourArea(cnt)
        # v.append(area)
        # avg_area = Average(v)
        x, y, w, h = cv2.boundingRect(cnt)
        X.append(x)
        Y.append(y)
        X.append(x+w)
        Y.append(y+h)
        wd.append(w)
        all.append([x,y])
        all.append([x+w,y+h])
        # rect_area.append(w * h)
        rect_points.append([x,y,w,h])
        # wd.append(w)
        cv2.rectangle(source, (x, y), (x + w, y + h), (0, 255, 0), 2)
        he.append(h)

Y=np.array(Y)
kmeans = KMeans(init='k-means++',n_clusters=15).fit(Y.reshape(-1, 1))
print(kmeans.labels_)
plt.scatter(X,Y,c=kmeans.labels_)

# plt.scatter(X1,Y1,c=kmeans.labels_)
# plt.scatter(kmeans)
plt.show()