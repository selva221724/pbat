![alt text](images/hsv.png)

# PBAT


## Plot Boundary Automation Tool

## Abstract 

The monitoring and tracking the Crop Plots was the current difficulties in the research farm fields. The research & development was undergoing the fundamental image processing techniques to come up with the solution to automate the boundary plotting around the crops filed to evaluate the range and rows. There were multiple iterations in which the algorithm was refining to a scalable solution. The Data was given to the team was based on NIR & RGB data. The first set of development was focused on the NIR data to extract the chlorphil feature from the image. The accuracy was saturated at one point where the manual intervention can only take up to the next level. Then, the HSV slicing method was developed to slice the background from the image. The methods gave a promising result to refine the algorithm more towards it. There were still missing plots in the generated boxes, then the logical way of boundary generation was developed to improve the accuracy.  The current methods can able toe automatically pick put the crops and generated the boxes around it. The manual intervention can be reviewed on the boxes using the front end tools given in the web application to achieve the 100% box generation.

#### Python Packages used in this Repository
- OpenCV
- Numpy 
- pyKML
- simplekml
- gdal 
- pyproj
- skimage.io

## Initial Development  
- The initial development  was on NIR data and used multiple image processing techniques to arrive at the detection
- The process was involved gaussian blur, binary thresholding, closing & dilation. 
- The feature extraction came with more distorted and missing detection
- The accuracy was meeting the minimum criteria, but it involved multiple manual interventions to run. That's the root cause to change the direction towards the other method to develop

## Large Scale Ortho Processing 
- During the initial stage, the algorithm was performed in the small scale cropped image. After the algorithm was built, image processing was performed on a large scale image. 
- The scripts were developed as a Qgis script to perform the detection and return as a .kml file. 
- The recent adoption was completely independent of Qgis, which is fully developed in python


## HSV Slicing Method on RGB Data
- The HSV slicing method was the other approach to extract the interested color feature which directed the focus towards this method. The Algorithm flows as follows, 
- **HSV Slicing:** The RGB image will be converted into the HSV image , Algorithm will slice the Background/Soil from the Orthomosaic using predefined HSV value for Green Color Range.
- **Binary Thresholding:** Once the extracted Crops Mask generated, it will perform the binary thresholding which will replace the other background pixels into the zero. 
- **Contouring:** From the binary image, the contours can be extracted  
- **Features Extraction:** Depending on the average crop area , the extracted contours filtered out
- **Boundary Box Generation:** The boundary was identified and Generated using the maximum and minimum cartesian coordinates of the contour
- **Pixel to Global:** The Pixel coordinates of boundary boxes will be converted into the Global coordinates and it will be exported as a .KML file 
- **UI:** The .KML will be dumped into the Database which will flow into the Front-End UI 


## Process Flow
![alt text](images/flow.png)

## To Run DEMO
- To Run the DEMO, install the required packages in the python and Run the Plot_boundary_Generation.py 
- This will generate the "out.jpg" and "Plot_Boundary.kml" files
- The DEMO will run the demo raster given in the folder. if the user needs to use the other rasters, the number of clusters needs to be changed in the script 


## Docker Container
![alt text](images/download.png)

The Docker container can be deployed in two methods,

1. Deploying the Docker Image from pre-built Image(.tar) file (Fastest approach) 
2. Building the Docker Image using the Docker file

### 1. Deploying the Docker Image from pre-built Image

- Please use the below link to download the prebuilt Image and It can be loaded into the Docker and deployed 
- [Click This Link To Download Docker Image](https://drive.google.com/file/d/1E1ZS8HPktfUiZ2HLtvLll5onovKg75tH/view?usp=sharing)
- Once the image file downloaded, go to the path which contains image and open the terminal and execute 
```bash
$ sudo docker load -i 'pbat.tar' 
```
- Now, the docker image is ready to run 
- The src should be the PBAT repo docker path
```bash
$ sudo docker run -it --mount src="PATH to pbat Repo/Docker",target=/test_container,type=bind  -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix  pbat 
```
- it will open the docker container shell and run the script 
```bash
$ python Plot_boundary_Generation.py
```
- It will generate the output kml in **test_container/Ortho** directory  

- To Run the Gaps detection on Demo Sweetcorn Ortho
```bash
$ python Gaps_Detector.py
```
- It will generate the output kml in **test_container/gaps** directory  

### 2. Building the Docker Image using the Docker file

Please follow the instructions to Build & Run the Docker

Commands to use the Display for docker:
```bash
$ sudo apt-get install x11-xserver-utils
$ xhost +
```
In Docker Directory, run this command to build the env 
```bash
$ sudo docker build -t pbat .
```

Once the Docker built the environment,run this to execute the script 
```bash
$ sudo docker run -it --mount src="$(pwd)",target=/test_container,type=bind  -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix  pbat
```
The source ortho should be in the Source directory to Execute 

The Script will open a dialog box one by one to select the raster & output path.

