import cv2
import numpy as np
from matplotlib import pyplot as plt
from skimage.io import imread
from osgeo import gdal
from pyproj import Proj, transform
import simplekml
from sklearn.cluster import AgglomerativeClustering


def detection(img):
    hsv = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)
    mask = cv2.inRange(hsv, (30, 20, 0), (80, 255, 255))
    imask = mask > 0
    green = np.zeros_like(img, np.uint8)
    green[imask] = img[imask]
    gray = cv2.cvtColor(green, cv2.COLOR_BGR2GRAY)
    ret, threshold = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    contours, _ = cv2.findContours(threshold, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)

    out_img = img.copy()

    area =[]
    for i in contours:
        area.append(cv2.contourArea(i))

    coords =[]
    points =[]
    for cnt in contours:
        area = cv2.contourArea(cnt)
        if 30000 < area < 120000:
            x,y,w,h = cv2.boundingRect(cnt)
            coords.append([(x+8,y),(x+w-8,y+h)])
            points.append((x+8, y))
            cv2.rectangle(out_img, (x+8, y), (x + w -8, y + h), (0, 255, 0), 3)

    cv2.imwrite('out.jpg', out_img)
    return coords , points


def Pixel2world(geoMatrix, x, y):
    ulX = geoMatrix[0]
    ulY = geoMatrix[3]
    xDist = geoMatrix[1]
    yDist = geoMatrix[5]
    coorX = (ulX + (x * xDist))
    coorY = (ulY + (y * yDist))
    return (coorX, coorY)


def KMLgen(features,KML_name):
    kml = simplekml.Kml()
    i = 0
    for row in features:
        pol = kml.newpolygon(outerboundaryis=[row[0],row[1],row[2],row[3],row[0]])

        # pol.description = des[i]
        i+=1
    kml.save(KML_name)

# ========================================= Clustering ==============================================


def clustering(img , points , number_of_cluster):

    points = np.array(points)

    X = np.array(points)
    # X = np.array([[i[1],i[0]] for i in points])


    cluster = AgglomerativeClustering(n_clusters=number_of_cluster, affinity='euclidean', linkage='single')
    labels = cluster.fit_predict(X)

    for i in set(cluster.labels_):
        index = cluster.labels_ == i
        plt.plot(X[index,0], X[index,1], 'o')
    plt.show()

    cluster_points = {i: X[np.where(cluster.labels_ == i)] for i in range(cluster.n_clusters)}
    cluster_points = list(cluster_points.values())
    cluster_points =[i.tolist() for i in cluster_points]

    a,b,c = img.shape

    cluster_points_co=[]
    for i in cluster_points:
        cluster =[]
        for j in i:
            cluster.append(j)
        cluster.append([0,i[0][1]])
        cluster.append([b,i[-1][1]])
        cluster_points_co.append(cluster)

    cluster_points = [sorted(i, key=lambda k: [k[0], k[1]]) for i in cluster_points_co]

    for j in range(len(cluster_points)):
        for i in range(len(cluster_points[j])-1):
            cv2.line(img,tuple(cluster_points[j][i]),tuple(cluster_points[j][i+1]), (0, 0, 0), 20)

    plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))


def set_crs(x, y,inProj ,outProj ):
    x2, y2 = transform(inProj, outProj, x, y)
    return x2, y2


def main_run(img_path, logical_improvement, number_of_cluster = None):
    img = imread(img_path)
    coords, points = detection(img)
    
    if logical_improvement:
        img = imread(img_path)
        clustering(img, points, number_of_cluster)
        coords, points = detection(img)

    ds=gdal.Open(img_path)
    prj=ds.GetProjection()
    geoTrans = ds.GetGeoTransform()
    v = prj.split(',')
    v = v[-1]
    v = v.split('"')
    projection = v[1]

    inProj = Proj(init='epsg:'+ projection)
    outProj = Proj(init='epsg:4326')




    box_out_global = []
    for j in coords:
        box_out_global.append([Pixel2world(geoTrans,i[0],i[1]) for i in j])

    box_out_global_crs =[]
    for j in box_out_global:
        box_out_global_crs.append([set_crs(i[0],i[1],inProj,outProj) for i in j])


    box_out_global_crs = [((i[0]),(i[0][0],i[1][1]),(i[1]),(i[1][0],i[0][1])) for i in box_out_global_crs]


    KMLgen(box_out_global_crs,'test_container/Ortho/Plot_Boundary.kml')


main_run('test_container/Ortho/DEMO_Raster.tif',logical_improvement=True,number_of_cluster=4)