Please follow the instructions to Build & Run the Docker

1. Commands to use the Display:
$ sudo apt-get install x11-xserver-utils
$ xhost +

2. In Docker Directory, run this command 
$ sudo docker build -t pbat .

3. Once the Docker built the environment, 
$ sudo docker run -it --mount src="$(pwd)",target=/test_container,type=bind  -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix  pbat

4. The source ortho should be in the Source directory to Execute 
